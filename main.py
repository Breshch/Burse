import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier, SGDRegressor
from sklearn.metrics import accuracy_score, mean_squared_error

seed = 124356


def get_df():
    df = pd.read_csv('data.csv', sep=';', parse_dates=[['<DATE>', '<TIME>']])

    df['datetime'] = df['<DATE>_<TIME>']
    df['price'] = df['<CLOSE>']

    df.drop(['<DATE>_<TIME>', '<TICKER>', '<PER>', '<VOL>', '<OPENINT>', '<OPEN>', '<HIGH>', '<LOW>', '<CLOSE>'], axis=1, inplace=True)

    df = df[(df['datetime'].dt.weekday < 5) &
            (df['datetime'].dt.hour * 60 + df['datetime'].dt.minute >= 10 * 60)]

    return df


def get_prices(df, n_prices):
    df = df[(df['datetime'].dt.hour * 60 + df['datetime'].dt.minute <= ((18 * 60 + 40) - n_prices))]
    prices = df['price'].values
    return prices


def get_changing(x):
    changing = 0
    market_state = -2
    last_changing = 0
    for el_i in range(1, len(x)):
        if x[el_i - 1] < x[el_i]:
            if market_state == 1:
                last_changing += 1
            elif market_state == 0:
                market_state = 1
                continue
            elif market_state == -1:
                last_changing = 0
                market_state = 1
            else:
                market_state = 1

        elif x[el_i - 1] > x[el_i]:
            if market_state == -1:
                last_changing += 1
            elif market_state == 0:
                market_state = -1
                continue
            elif market_state == 1:
                last_changing = 0
                market_state = -1
            else:
                market_state = -1

        if x[el_i - 1] == x[el_i]:
            continue

        changing += (last_changing + 1) * (x[el_i] - x[el_i - 1])
    return changing


def get_max_min_prices(x):
    x_max = max(x) - x[0]
    x_min = abs(min(x) - x[0])
    return x_max, x_min


def get_features(x):
    changing = get_changing(x)
    max_price, min_price = get_max_min_prices(x)

    return [changing, max_price, min_price]


def get_dataset(prices, n_prices, time_to_trade):
    X = []
    y = []
    for i in range(0, len(prices) - (n_prices + time_to_trade)):
        x = prices[i:i + n_prices]

        features = get_features(x)

        X.append(features)
        # y.append(prices[i + (n_prices + time_to_trade)] - prices[i + n_prices])

        target = 0
        if prices[i + (n_prices + time_to_trade)] > prices[i + n_prices]:
            target = 1
        elif prices[i + (n_prices + time_to_trade)] < prices[i + n_prices]:
            target = -1

        y.append(target)

        # print(x, changing, datetimes[i])

    X = np.array(X)
    y = np.array(y)
    return X, y


# 1 1 0 0 1
# 1 1 1 1 1

# 12.2 .12.12.12.3.12.4.12.5
# 12.19


def get_total(prices, clf, n_prices, time_to_trade):
    total = 0
    current_index = 0
    while (current_index + n_prices + time_to_trade) < len(prices):
        x = prices[current_index:n_prices + current_index]

        features = [get_features(x)]
        decision = clf.predict(features)[0]

        trade_open_price = prices[n_prices + current_index]
        trade_close_price = prices[n_prices + time_to_trade + current_index]
        dif_price = trade_close_price - trade_open_price

        if decision == 1:
            total += dif_price
        elif decision == -1:
            total -= dif_price

        # print('current total:', total)

        current_index += n_prices + time_to_trade

    return total


def fit_model(X_train, y_train, X_val, y_val):
    clf = SGDClassifier(max_iter=10, random_state=seed)
    # clf = SGDRegressor(max_iter=10, random_state=seed)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_val)

    return clf, accuracy_score(y_val, y_pred)
    # return mean_squared_error(y_val, y_pred)


# 1 2 3 4 5
# 2 3 4 5 6
# 3 4 5 6 7


# 1 2 3 4 5


df = get_df()

max_acc = 0
max_acc_total = 0

max_total = 0
max_total_acc = 0

best_n_prices_acc = 0
best_time_to_trade_acc = 0

best_n_prices_total = 0
best_time_to_trade_total = 0
# best_clf = None

for n_prices in range(5, 1001, 1):
    for time_to_trade in range(1, 1001, 1):
        prices = get_prices(df, n_prices)
        prices_train, prices_val = train_test_split(prices, shuffle=False, test_size=0.4, random_state=seed)

        X_train, y_train = get_dataset(prices_train, n_prices, time_to_trade)
        X_val, y_val = get_dataset(prices_val, n_prices, time_to_trade)

        clf, acc = fit_model(X_train, y_train, X_val, y_val)
        total = get_total(prices_val, clf, n_prices, time_to_trade)

        if acc > max_acc:
            max_acc = acc
            max_acc_total = total

            best_n_prices_acc = n_prices
            best_time_to_trade_acc = time_to_trade

        if total > max_total:
            max_total = total
            max_total_acc = acc

            best_n_prices_total = n_prices
            best_time_to_trade_total = time_to_trade

    # print('n_prices:', n_prices)


print('acc:', max_acc, max_acc_total, best_n_prices_acc, best_time_to_trade_acc)
print('tot:', max_total, max_total_acc, best_n_prices_total, best_time_to_trade_total)


# prices = get_prices(df, best_n_prices)


# print('total:', total)




# 0.581481481481 25 90
# 0.587903639159 70 100


#acc: 0.587903639159 29.2 70 100
#tot: 31.5 0.560464078536 10 30
#
#

# df["price"].plot()
# plt.show()





